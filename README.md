# renamer

The small utility to rename files in the provided folder.

The file name splits by delimiters "-" and "_" and removes all duplicates of the same words.

The result file name contains only "-" delimiter.

If the target file name already exists then the -1, -2, -3, etc. suffix added 


To run probram:

java -jar renamer-1.0.0-jar-with-dependencies.jar Renamer folder-with-files 2> not-renamed.txt > renamed.txt

# column renamer

the utility to process the csv files and remove duplicate words in the provided column number

There are 3 input parameters: input csv file name, output csv filename, zero-based column number.

To run program:

java -jar renamer-1.0.0-jar-with-dependencies.jar ColumnRenamer INPUT.csv OUTPUT.csv 2 > result.csv
