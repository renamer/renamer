import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Renamer {


    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please provide the folder name to rename files in this folder");
            System.exit(1);
        }

        try (Stream<Path> paths = Files.walk(Paths.get(args[0]))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(Renamer::rename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void rename(Path path) {
        String file = path.getFileName().toString();
        Path folder = path.getParent();
        StringBuilder result = new StringBuilder();
        int extentionIndex = file.lastIndexOf(".");
        String fileNameWithoutExtention = file.substring(0, extentionIndex);
        String extention = file.substring(extentionIndex);
        String[] fileParts = fileNameWithoutExtention.split("[-_]");

        result.append(folder).append(File.separator);
        Set<String> words = new HashSet<>(Arrays.asList(fileParts));
        for (String part : fileParts) {
            if (words.contains(part)) {
                if (result.length() != folder.toString().length() + 1){
                    result.append("-");
                }
                result.append(part);
                words.remove(part);
            }
        }

        File oldFile = new File(path.toString());
        if (!path.toString().equals(result.toString() + extention)) {

            String suffix = "";
            int counter = 1;
            while (true) {
                File newFile = new File(result.toString()+suffix + extention);
                if (newFile.exists()) {
                    suffix = "-" + counter;
                    counter++;
                } else {
                    System.out.println(file);
                    System.out.println(newFile.getName());
                    System.out.println();
                    oldFile.renameTo(newFile);
                    break;
                }
            }
        } else {
            System.err.println(oldFile.getName());
        }
    }

}
