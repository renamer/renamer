import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ColumnRenamer {


    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Please provide the path to input CSV file");
            System.exit(1);
        }


        if (args.length < 2) {
            System.out.println("Please provide the path to output CSV file");
            System.exit(1);
        }


        if (args.length < 3) {
            System.out.println("Please provide the column number to process, the first column should have the 0 number");
            System.exit(1);
        }

        String csvFile = args[0];
        String outputFile = args[1];
        int columnNumber= 0;
        try {
            columnNumber = new Integer(args[2]);
        } catch (NumberFormatException nfe){
            System.out.println("Invalid column number provided: " + args[2]);
            System.exit(1);
        }

        CSVReader reader;
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new FileWriter(outputFile), ICSVParser.DEFAULT_SEPARATOR,
                    '\'',
                    ICSVParser.DEFAULT_ESCAPE_CHARACTER);
            reader = new CSVReader(new FileReader(csvFile), ICSVParser.DEFAULT_SEPARATOR,
                    '\'',
                    ICSVParser.DEFAULT_ESCAPE_CHARACTER);
            reader.setMultilineLimit(1);
            String[] line;
            Map<String, Integer> map = new HashMap<>();
            while ((line = reader.
                    readNext()) != null) {
                String str = rename(line[columnNumber]);
                Integer count = map.get(str);
                String index = "";
                if (count == null){
                    map.put(str, 1);
                } else {
                    index = String.valueOf(count);
                    count = count+1;
                    map.put(str, count);
                }
                line[columnNumber] = str + " " + index;
                writer.writeNext(line, false);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null){
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }


    public static String rename(String content) {
        StringBuilder result = new StringBuilder();

        String[] wordsArray = content.split(" ");


        Set<String> words = new HashSet<>(Arrays.asList(wordsArray));
        for (String part : wordsArray) {
            if (words.contains(part)) {
                result.append(part);
                result.append(" ");
                words.remove(part);
            }
        }
        if (result.length() > 0) {
            result.setLength(result.length() - 1);
        }
        return result.toString();
    }

}
